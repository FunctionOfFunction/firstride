$("#driver-application-form button").click(function() {
  $(".error").html("");

  $(".loading").addClass("on");

  var url = "/api/";
  $.post(
    url,
    {
      first_name: $("#first_name").val(),
      last_name: $("#last_name").val(),
      email: $("#email").val(),
      phone_number: $("#phone_number").val(),
      dob: $("#dob").val(),
      address: $("#address").val()
    },
    function(data) {
      $(".loading").removeClass("on");
      if (data.errors.length) {
        $(".error-found").html("Fix errors and resubmit");
        data.errors.forEach(error => {
          $(".error-found").html("");
          $("." + error.field + " .error").html(error.verbose);
        });
      } else {
        window.location.replace("/submitted");
      }
    }
  );
});
