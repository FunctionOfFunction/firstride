<?php

require_once('../../included/constants.php');
require_once('../../included/fields.php');
require_once('../../included/functions.php');

header('Content-Type: application/json');

echo json_encode(submit($post_fields,OUTPUT_CSV));