<?php

$post_fields = [
    'first_name'        => ['name'],
    'last_name'         => ['name'],
    'dob'               => ['dob'],
    'phone_number'      => ['phone_number'],
    'email'             => ['email'],
    'address'             => ['address'],
];