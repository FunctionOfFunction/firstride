<?php

function validate_form($post_fields){
    $errors = [];
    foreach ($post_fields as $field => $checks) {
        foreach ($checks as $check) {
            if ($value = get_value($field)){
                if(!$check($field, $value)){
                    $method = $check.'_error';
                    $errors[] = [
                        'field' => $field, 
                        'verbose' => $method($field, $value)
                    ];
                }
            }else {
               $errors[] = [
                   'field' => $field, 
                   'verbose' =>no_value($field)
                ];
            }
        }
    }
    return $errors;
}

function escape_commas($string){
    return  strip_char($string, ',');
}

function strip_char($subject, $char){
    return  implode(" ",explode($char,$subject));
}

function submit($post_fields, $path){
    $errors = validate_form($post_fields);

    if (count($errors) == 0) {
        $line = '';
        foreach ($post_fields as $field => $check) {
            $line .= escape_commas(get_value($field)).',';
        }
        $line .= PHP_EOL;
        write_file($path, $line);
        send_mail_using_sendgrid(
            get_value('email'), 
            'Partnership Application  was received', get_thanks_message(
                get_value('first_name'), 
                get_value('last_name')
            )
        );
    }

    return ['errors' => $errors];
}

function write_file($file_name, $content, $operation="a"){
		$file = fopen($file_name, $operation);
		if (fwrite($file,$content)) {
			fclose($file);
			return true;
		}else {
			return false;
		}
}

function get_value($field){
    return @$_POST[$field];
}

function no_value($field){
    return ucwords(strip_char($field, '_')).' can not be empty';
}

function name($field, $value){
    return strlen($value) >= SHORTEST_NAME; 
}

function name_error($field, $value){
   return 'This name is incorrect';
}

function is_valid_phone_length($value){
    $phone_number_len = strlen($value);
    return $phone_number_len >= PHONE_MIN_LEN 
        &&  $phone_number_len <= PHONE_MAX_LEN ; 
}

function is_valid_phone_format($value){
    return preg_match(PHONE_CHARS, $value);
}

function phone_number($field, $value){
    return is_valid_phone_format($value) 
        && is_valid_phone_length($value);
}

function phone_number_error($field, $value){
   return 'This phone Number is incorrect';
}

function email($field, $value){
     return filter_var($value, FILTER_VALIDATE_EMAIL);
}

function email_error($field, $value){
   return 'This email is incorrect';
}

function dob($field, $value){
     return strtotime(OLD_ENOUGH_AGE_IN_SEC) >= strtotime($value);
}

function dob_error($field, $value){
   return 'You must be at least 18 years old';
}

function address($field, $value){
     return strlen($value) > MIN_ADDRESS_LENGTH;
}

function address_error($field, $value){
   return 'This Address is not acceptable, Please be more specific';
}


function sendgrid_headers(){
    return [
        'Authorization: Bearer '.SENDGRID_API_KEY,
        'Content-Type: application/json',
    ];
}

function get_thanks_message($firstname, $last_name){
    return
    '<body style="background: #2fcc35; text-align: center; padding: 64px 0">
    <div style="background: #ffffff; color: #333; width: 80%; margin-left: 10%; padding:32px; text-align : left; border-radius: 8px; margin-bottom: 32px;">
    <h4>Dear '.$firstname .' '.$last_name .',</h4>
    <p>
    Your application for Rider-partnership was succuessfully submitted. You are on your way to be coming your own boss. We will be reaching out to you soon. 
    Kindly help tell others about 1st-ride partnership</p>
    <br>
    Regards,<br>
    1st-ride Team.
    <div>
    <small>&copy; 1st-ride, 2019. All rights reserved<small><br>
    <small>4, Mojosola Kazeem Str, Amuwo Odofin Estate, Lagos<small>
    </body>';
}

function sendgrid_post_data($email, $subject, $body){
   return json_encode(
        [
            'personalizations' =>[
                [
                    'to' => [
                        ['email' => $email]
                    ] 
                ]
            ],
            'from' => [
                'email' => SEND_EMAIL_ADDR
            ],
            'subject' => $subject,
            'content' => [
                [
                    "type" => 'text/html',
                    "value" => $body
                ]
            ]
        ]
    );
}

function send_mail_using_sendgrid($email, $subject, $body){
    $ch = curl_init();
    curl_setopt($ch,  CURLOPT_URL, SENDGRID_API_ENDPOINT);
    curl_setopt($ch,  CURLOPT_POST, 1);
    curl_setopt($ch,  CURLOPT_POSTFIELDS, sendgrid_post_data($email, $subject, $body));
    curl_setopt($ch,  CURLOPT_HTTPHEADER, sendgrid_headers());
    curl_setopt($ch,  CURLOPT_FOLLOWLOCATION, 1 );
    $response = @curl_exec($ch);
    @curl_close($ch);
    
}